/* uid_t, gid_t */
#include <sys/types.h>

void h(char* /* home */,
       uid_t /* uid */, gid_t /* gid */, mode_t /* mode */);

void d(char* /* home */,
       char* /* subdir*/,
       uid_t /* uid */, gid_t /* gid */, mode_t /* mode */);

void p(char* /* home */,
       char* /* fifo */,
       uid_t /* uid */, gid_t /* gid */, mode_t /* mode */);

void c(char* /* home */,
       char* /* subdir */,
       char* /* file */,
       uid_t /* uid */, gid_t /* gid */, mode_t /* mode */);
