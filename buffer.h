#ifndef BUFFER_H
#define BUFFER_H

/* ssize_t */
#include <sys/types.h>

#if (defined(__POSIX_VISIBLE) && (__POSIX_VISIBLE >= 199901)) || \
    (defined(_POSIX_VERSION) && (_POSIX_VERSION >= 199901L))
# define HAVE_POSIX_TYPED_READ_WRITE
#endif

typedef ssize_t ssize_func_t();
typedef ssize_t reader_t(int,void *,size_t);
typedef ssize_t writer_t(int,const void *,size_t);

/*
 * buffer is a uni-directional structure, which can
 * have single "op" function pointer, usually it is
 * a pointer to read(2) or write(2), without checking
 * the types of the arguments.
 *
 * considering POSIX have different types of the argument
 * for read(2) and write(2), the member "op" is repalced
 * by a union which can be typed as read(2)-like function,
 * write(2)-like function, or generator with no argument.
 *
 */
typedef struct buffer {
  char *x;
  unsigned int p;
  unsigned int n;
  int fd;
  union {
    ssize_func_t *gen;
    reader_t *reader;
    writer_t *writer;
  } u;
} buffer;

#ifdef HAVE_POSIX_TYPED_READ_WRITE
# define BUFFER_INIT(op,fd,buf,len) { (buf), 0, (len), (fd), { .gen = (op) } }
# define BUFFER_INIT_READER(op,fd,buf,len) { (buf), 0, (len), (fd), { .reader = (op) } }
# define BUFFER_INIT_WRITER(op,fd,buf,len) { (buf), 0, (len), (fd), { .writer = (op) } }
#else
# define BUFFER_INIT(op,fd,buf,len) { (buf), 0, (len), (fd), { .gen = (op) } }
# define BUFFER_INIT_READER(op,fd,buf,len) BUFFER_INIT((op), (fd), (buf), (len))
# define BUFFER_INIT_WRITER(op,fd,buf,len) BUFFER_INIT((op), (fd), (buf), (len))
#endif

#define BUFFER_INSIZE 8192
#define BUFFER_OUTSIZE 8192

extern void buffer_init(buffer *,ssize_func_t,int,char *,unsigned int);
extern void buffer_init_reader(buffer *,reader_t,int,char *,unsigned int);
extern void buffer_init_writer(buffer *,writer_t,int,char *,unsigned int);

extern int buffer_flush(buffer *);
extern int buffer_put(buffer *,char *,unsigned int);
extern int buffer_putalign(buffer *,char *,unsigned int);
extern int buffer_putflush(buffer *,char *,unsigned int);
extern int buffer_puts(buffer *,char *);
extern int buffer_putsalign(buffer *,char *);
extern int buffer_putsflush(buffer *,char *);

#define buffer_PUTC(s,c) \
  ( ((s)->n != (s)->p) \
    ? ( (s)->x[(s)->p++] = (c), 0 ) \
    : buffer_put((s),&(c),1) \
  )

extern int buffer_get(buffer *,char *,unsigned int);
extern int buffer_bget(buffer *,char *,unsigned int);
extern int buffer_feed(buffer *);

extern char *buffer_peek(buffer *);
extern void buffer_seek(buffer *,unsigned int);

#define buffer_PEEK(s) ( (s)->x + (s)->n )
#define buffer_SEEK(s,len) ( ( (s)->p -= (len) ) , ( (s)->n += (len) ) )

#define buffer_GETC(s,c) \
  ( ((s)->p > 0) \
    ? ( *(c) = (s)->x[(s)->n], buffer_SEEK((s),1), 1 ) \
    : buffer_get((s),(c),1) \
  )

extern int buffer_copy(buffer *,buffer *);

extern buffer *buffer_0;
extern buffer *buffer_0small;
extern buffer *buffer_1;
extern buffer *buffer_1small;
extern buffer *buffer_2;

#endif
