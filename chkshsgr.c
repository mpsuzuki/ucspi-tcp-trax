/* getgroups() */
#include <sys/types.h>
#include <unistd.h>

/* setgroups() */
#include <grp.h>

#include "exit.h"

int main()
{

#define GIDS_LEN 1
  union {
    gid_t g[GIDS_LEN];
    short x[4];
  } u = { 0 };

  if (getgroups(GIDS_LEN,u.g) == 0) if (setgroups(GIDS_LEN,u.g) == -1) _exit(1);
  _exit(0);
}
