#ifndef REMOTEINFO_H
#define REMOTEINFO_H

#include "stralloc.h"
#include "uint16.h"

extern int remoteinfo(stralloc *,char[4],uint16,char[4],uint16,unsigned int);

#endif
