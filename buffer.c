#include "buffer.h"

void buffer_init(buffer *s,ssize_func_t op,int fd,char *buf,unsigned int len)
{
  s->x = buf;
  s->fd = fd;
  s->u.gen = op;
  s->p = 0;
  s->n = len;
}

void buffer_init_reader(buffer *s,reader_t rd,int fd,char *buf,unsigned int len)
{
  s->x = buf;
  s->fd = fd;
  s->u.reader = rd;
  s->p = 0;
  s->n = len;
}

void buffer_init_writer(buffer *s,writer_t wr,int fd,char *buf,unsigned int len)
{
  s->x = buf;
  s->fd = fd;
  s->u.writer = wr;
  s->p = 0;
  s->n = len;
}
