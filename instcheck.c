#include <sys/types.h>
#include <sys/stat.h>
/* chdir() */
#include <unistd.h>

/* prototypes */
#include "instcheck.h"

#include "strerr.h"
#include "error.h"
#include "readwrite.h"
#include "exit.h"

extern void hier();

#define FATAL "instcheck: fatal: "
#define WARNING "instcheck: warning: "

void perm(char  *prefix1,
          char  *prefix2,
          char  *prefix3,
          char  *file,
          mode_t type,
          uid_t  uid,
          gid_t  gid,
          mode_t mode)
{
  struct stat st;

  if (stat(file,&st) == -1) {
    if (errno == error_noent)
      strerr_warn6(WARNING,prefix1,prefix2,prefix3,file," does not exist",0);
    else
      strerr_warn4(WARNING,"unable to stat .../",file,": ",&strerr_sys);
    return;
  }

  if ((uid != (uid_t)-1) && (st.st_uid != uid))
    strerr_warn6(WARNING,prefix1,prefix2,prefix3,file," has wrong owner",0);
  if ((gid != (gid_t)-1) && (st.st_gid != gid))
    strerr_warn6(WARNING,prefix1,prefix2,prefix3,file," has wrong group",0);
  if ((st.st_mode & 07777) != mode)
    strerr_warn6(WARNING,prefix1,prefix2,prefix3,file," has wrong permissions",0);
  if ((st.st_mode & S_IFMT) != type)
    strerr_warn6(WARNING,prefix1,prefix2,prefix3,file," has wrong type",0);
}

void h(char  *home,
       uid_t  uid,
       gid_t  gid,
       mode_t mode)
{
  perm("","","",home,S_IFDIR,uid,gid,mode);
}

void d(char  *home,
       char  *subdir,
       uid_t  uid,
       gid_t  gid,
       mode_t mode)
{
  if (chdir(home) == -1)
    strerr_die4sys(111,FATAL,"unable to switch to ",home,": ");
  perm("",home,"/",subdir,S_IFDIR,uid,gid,mode);
}

void p(char  *home,
       char  *fifo,
       uid_t  uid,
       gid_t  gid,
       mode_t mode)
{
  if (chdir(home) == -1)
    strerr_die4sys(111,FATAL,"unable to switch to ",home,": ");
  perm("",home,"/",fifo,S_IFIFO,uid,gid,mode);
}

void c(char  *home,
       char  *subdir,
       char  *file,
       uid_t  uid,
       gid_t  gid,
       mode_t mode)
{
  if (chdir(home) == -1)
    strerr_die4sys(111,FATAL,"unable to switch to ",home,": ");
  if (chdir(subdir) == -1)
    strerr_die6sys(111,FATAL,"unable to switch to ",home,"/",subdir,": ");
  perm(".../",subdir,"/",file,S_IFREG,uid,gid,mode);
}

int main()
{
  hier();
  _exit(0);
}
