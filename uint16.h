#ifndef UINT16_H
#define UINT16_H

typedef unsigned short uint16;

extern void uint16_pack(char[2],uint16);
extern void uint16_pack_big(char[2],uint16);
extern void uint16_unpack(char[2],uint16 *);
extern void uint16_unpack_big(char[2],uint16 *);

#endif
